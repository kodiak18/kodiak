package com.citi.training.trade.engine;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trade.bus.TradeService;
import com.citi.training.trade.dto.Trade;

@Component
public class TradeEngine {

    private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);

    @Autowired
    private TradeService tradeService;

    @Scheduled(fixedRate = 10000)
    public void sendTrades() {
        List<Trade> initTrades = tradeService.getTradesByState(Trade.TradeState.INIT);

        log.debug("Processing " + initTrades.size() + " new trades");
        for(Trade trade : initTrades) {
            trade.stateChange(Trade.TradeState.FILLED);
            tradeService.saveTrade(trade);
        }
    }
    
    
}
