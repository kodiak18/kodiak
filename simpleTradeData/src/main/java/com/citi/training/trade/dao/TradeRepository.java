package com.citi.training.trade.dao;

import java.util.List;

import com.citi.training.trade.dto.Trade;

public interface TradeRepository {

    public Trade saveTrade(Trade Trade);

    public Trade getTradeById(int Id);

    public List<Trade> getTradesByStock(String stock);

    public List<Trade> getTradesByState(Trade.TradeState state);

    public List<Trade> getAllTrades();

}
