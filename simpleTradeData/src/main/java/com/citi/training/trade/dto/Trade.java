package com.citi.training.trade.dto;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Trade {
    private static final Logger log = LoggerFactory.getLogger(Trade.class);

    public enum TradeType {
        BUY ("true"),
        SELL ("false");

        private final String xmlBuyValue;

        private TradeType(String xmlBuyValue) {
            this.xmlBuyValue = xmlBuyValue;
        }

        public String getXmlBuyValue() {
            return this.xmlBuyValue;
        }
    }

    public enum TradeState {
        INIT,
        WAITING_FOR_REPLY,
        FILLED,
        PARTIALLY_FILLED,
        CANCELED,
        DONE_FOR_DAY,
        REJECTED;
    }

    private int id = -1;
    private TradeType tradeType;
    private String stock;
    private TradeState state = TradeState.INIT;
    private LocalDateTime lastStateChange = LocalDateTime.now();

    public Trade() {}

    public Trade(String stock, TradeType tradeType) {
        this.stock = stock;
        this.setTradeType(tradeType);
    }

    public Trade(String stock, String tradeType) throws IllegalArgumentException {
        this(stock, TradeType.valueOf(tradeType));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TradeType getTradeType() {
        return tradeType;
    }

    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public TradeState getState() {
        return state;
    }

    public void setState(TradeState state) {
        this.state = state;
    }

    public LocalDateTime getLastStateChange() {
        return lastStateChange;
    }

    public void setLastStateChange(LocalDateTime lastStateChange) {
        this.lastStateChange = lastStateChange;
    }

    // to be called when state is changing - updates lastStateChanged
    // (setState() is used for obj creation etc..)
    public void stateChange(TradeState newState) {
        log.debug("State change to + " + newState + " on " + this);
        this.state = newState;
        this.lastStateChange = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Trade [id=" + id + ", tradeType=" + tradeType +
               ", stock=" + stock + ", state=" + state +
               ", lastStateChange=" + lastStateChange + "]";
    }

    @Override
    public boolean equals(Object trade) {
        if (trade == this) {
            return true;
        }

        // This implementation allows ids to be different
        return (trade instanceof Trade) &&
               getStock().equals(((Trade)trade).getStock()) &&
               getTradeType() == ((Trade)trade).getTradeType() &&
               getState() == ((Trade)trade).getState() &&
               getLastStateChange().equals(((Trade)trade).getLastStateChange());
    }
}
