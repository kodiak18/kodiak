package com.citi.training.trade.bus;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.trade.dao.TradeRepository;
import com.citi.training.trade.dto.Trade;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    public Trade saveTrade(Trade newTrade) {
        return tradeRepository.saveTrade(newTrade);
    }

    public Trade createTrade(String stock, String tradeType) {
        return tradeRepository.saveTrade(new Trade(stock, tradeType));
    }

    public Trade getTradeById(int id) {
        return tradeRepository.getTradeById(id);
    }

    public List<Trade> getTradesByStock(String stock) {
        return tradeRepository.getTradesByStock(stock);
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        return tradeRepository.getTradesByState(state);
    }

    public List<Trade> getAllTrades() {
        return tradeRepository.getAllTrades();
    }
}
