package com.citi.training.trade.dao;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.trade.dto.Trade;

@Repository("JdbcTradeRepository")
public class JdbcTradeRepository implements TradeRepository {

    private static final Logger log = LoggerFactory.getLogger(JdbcTradeRepository.class);

    @Value("${trade.table.name:trades}")
    private String tableName = "trades";

    private final String insertSQL = "INSERT INTO " + tableName + " (stock, lastStateChange, " +
                                     "tradeType, state) values (:stock, :lastStateChange, " +
                                     ":tradeType, :state)";

    private final String updateSQL = "UPDATE " + tableName + " SET stock=:stock, " +
                                     "lastStateChange=:lastStateChange, tradeType=:tradeType, state=:state " +
                                     "WHERE id=:id";

    private final String selectByIdSQL = "SELECT * FROM " + tableName + " WHERE id=?";
    private final String selectByStockSQL = "SELECT * FROM " + tableName + " WHERE stock=?";
    private final String selectByStateSQL = "SELECT * FROM " + tableName + " WHERE state=?";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // returns the object given
    // if id < -1 then it will be inserted, otherwise updated
    public Trade saveTrade(Trade trade) {
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(trade);

        // use string value of tradeType and state enums
        namedParameters.registerSqlType("tradeType", Types.VARCHAR);
        namedParameters.registerSqlType("state", Types.VARCHAR);

        if (trade.getId() < 0) {
            // insert
            log.debug("Inserting trade: " + trade);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(insertSQL,namedParameters, keyHolder);
            trade.setId(keyHolder.getKey().intValue());
        } else {
            log.debug("Updating trade: " + trade);
            namedParameterJdbcTemplate.update(updateSQL, namedParameters);
        }
        log.info("JdbcRepo returning trade: " + trade);
        return trade;
    }

    public Trade getTradeById(int id) {
        log.debug("JdbcTradeRepo getById: " + id);
        List<Trade> trades = jdbcTemplate.query(selectByIdSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                id);

        log.debug("Query for id <" + id + "> returned list: " + trades);
        return trades.get(0);
    }

    public List<Trade> getTradesByStock(String stock) {
        log.debug("JdbcTradeRepo getByStock: " + stock);
        List<Trade> trades = jdbcTemplate.query(selectByStockSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                stock);

        log.debug("Query for stock <" + stock + "> returned list of size: " + trades.size());
        return trades;
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        log.debug("JdbcTradeRepo getByState: " + state);
        List<Trade> trades = jdbcTemplate.query(selectByStateSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                state.toString());

        log.debug("Query for state <" + state + "> returned list of size: " + trades.size());
        return trades;
    }

    public List<Trade> getAllTrades() {
        log.debug("JdbcTradeRepo getAll");
        List<Trade> trades = jdbcTemplate.query(selectAllSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class));

        log.debug("Query for all returned list of size: " + trades.size());
        return trades;
    }
}
