package com.citi.training.trade.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.citi.training.trade.dao.InMemTradeRepository;
import com.citi.training.trade.dao.TradeRepository;
import com.citi.training.trade.dto.Trade;

public class InMemTradeRepositoryTest {

    private TradeRepository tradeRepository = new InMemTradeRepository();
    private Trade testTrade; 

    @Before
    public void createTrade() {
        this.testTrade = new Trade("AMZN", "BUY");
    }

    @Test
    public void test_savingAndRetrieving() {
        assertTrue(tradeRepository.getAllTrades().size() == 0);

        int orig_id = testTrade.getId();
        Trade newTrade = tradeRepository.saveTrade(testTrade);
        assertFalse(orig_id == newTrade.getId());
        assertTrue(newTrade == testTrade);

        List<Trade> amznTrades = tradeRepository.getTradesByStock("AMZN");
        assertTrue(amznTrades.get(0) == newTrade);

        Trade foundById = tradeRepository.getTradeById(newTrade.getId());
        assertTrue(foundById == newTrade);

        List<Trade> foundByState = tradeRepository.getTradesByState(Trade.TradeState.INIT);
        assertTrue(foundByState.size() == 1);
        assertTrue(foundByState.get(0).getState() == Trade.TradeState.INIT);
    }

}
