package com.citi.training.trade.bus;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trade.TestApplication;
import com.citi.training.trade.bus.TradeService;
import com.citi.training.trade.dao.TradeRepository;
import com.citi.training.trade.dto.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestApplication.class)
public class TradeServiceTest {

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeRepository mockTradeRepository;

    private Trade testTrade; 

    @Before
    public void createTrade() {
        this.testTrade = new Trade("AAPL", "SELL");
    }

    @Test
    public void test_addTrade_callsSave() {
        // when tradeService.saveTrade is called then
        // mock should return the Trade it was given
        when(mockTradeRepository.saveTrade(ArgumentMatchers.any(Trade.class)))
            .thenAnswer(save -> save.getArguments()[0]);

        Trade newTrade = tradeService.saveTrade(this.testTrade);

        // verify service returns the given trade
        assertTrue(newTrade == this.testTrade);
        assertTrue(newTrade.equals(this.testTrade));
        verify(mockTradeRepository).saveTrade(this.testTrade);
    }

    @Test
    public void test_createTrade_callsSave() {
        // when tradeService.saveTrade is called then
        // mock should return the Trade it was given
        when(mockTradeRepository.saveTrade(ArgumentMatchers.any(Trade.class)))
            .thenAnswer(save -> save.getArguments()[0]);

        Trade newTrade = tradeService.createTrade(testTrade.getStock(),
                                                  testTrade.getTradeType().toString());

        // verify the service returned a new trade
        assertFalse(newTrade == this.testTrade);

        // set lastStateChange time to be the same on both
        newTrade.setLastStateChange(this.testTrade.getLastStateChange());
        assertTrue(newTrade.equals(this.testTrade));
        verify(mockTradeRepository).saveTrade(newTrade);
    }

    @Test
    public void test_getTradeById() {
        when(mockTradeRepository.getTradeById(1)).thenReturn(testTrade);
        Trade newTrade = tradeService.getTradeById(1);
        assertTrue(newTrade == this.testTrade);
        verify(mockTradeRepository).getTradeById(1);
    }

    @Test
    public void test_getTradeByName() {
        when(mockTradeRepository.getTradesByStock("AAPL"))
            .thenReturn(new ArrayList<Trade>(Arrays.asList(testTrade)));

        List<Trade> trades = tradeService.getTradesByStock("AAPL");
        assertTrue(trades.get(0) == this.testTrade);
        verify(mockTradeRepository).getTradesByStock("AAPL");
    }

    @Test
    public void test_getTradeByState() {
        when(mockTradeRepository.getTradesByState(Trade.TradeState.INIT))
            .thenReturn(new ArrayList<Trade>(Arrays.asList(testTrade)));

        List<Trade> trades = tradeService.getTradesByState(Trade.TradeState.INIT);
        assertTrue(trades.get(0) == this.testTrade);
        verify(mockTradeRepository).getTradesByState(Trade.TradeState.INIT);
    }

    @Test
    public void test_getAllTrades() {
        when(mockTradeRepository.getAllTrades())
        .thenReturn(new ArrayList<Trade>(Arrays.asList(testTrade)));

        List<Trade> trades = tradeService.getAllTrades();
        assertTrue(trades.get(0) == this.testTrade);
        verify(mockTradeRepository).getAllTrades();
    }
}
