package com.citi.training.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.rest.controllers.TradeRestController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApplicationTests {

    @Autowired
    private TradeRestController tradeController;

    @Test
    public void test_contextLoads_sanityCheck() throws Exception {
        assert (tradeController != null);
    }

}
