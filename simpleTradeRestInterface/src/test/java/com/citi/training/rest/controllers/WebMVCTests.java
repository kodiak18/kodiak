package com.citi.training.rest.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trade.bus.TradeService;
import com.citi.training.trade.dto.Trade;

/* Only tests controller, service is mocked */

@RunWith(SpringRunner.class)
@WebMvcTest
public class WebMVCTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService mockTradeService;

    @Test
    public void test_createTrade_sanityCheck() throws Exception {
        when(mockTradeService.saveTrade(ArgumentMatchers.any(Trade.class)))
            .thenReturn(TestUtil.testTrade);

        MvcResult result = this.mockMvc
                .perform(post("/trades")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.testJson()))
                .andExpect(status().isCreated()).andReturn();

        TestUtil.assertUnexpectedResult("createEmployee expected",
                                        TestUtil.testTrade, result);
    }

    @Test
    public void test_getAll_SanityCheck() throws Exception {
        when(mockTradeService.getAllTrades()).thenReturn(TestUtil.tradeList);

        MvcResult result = this.mockMvc.perform(get("/trades"))
                           .andExpect(status().isOk()).andReturn();
        TestUtil.assertUnexpectedResult("getAll expected", TestUtil.testTrade,
                                        result);
    }

    @Test
    public void test_getByStock_sanityCheck() throws Exception {
        when(mockTradeService.getTradesByStock(TestUtil.testTrade.getStock()))
                .thenReturn(TestUtil.tradeList);

        MvcResult result = this.mockMvc
                           .perform(get("/trades/" + TestUtil.testTrade.getStock()))
                           .andExpect(status().isOk()).andReturn();
        TestUtil.assertUnexpectedResult("getAll expected", TestUtil.testTrade,
                                        result);
    }

    @Test
    public void test_findById_sanityCheck() throws Exception {
        int idToFind = 0;
        when(mockTradeService.getTradeById(idToFind)).thenReturn(TestUtil.testTrade);

        MvcResult result = this.mockMvc.perform(get("/trades/" + idToFind))
                .andExpect(status().isOk()).andReturn();

        TestUtil.assertUnexpectedResult("findById expected", TestUtil.testTrade,
                result);
    }
}
